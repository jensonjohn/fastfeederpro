package com.jenson.fastfeederpro.controllers;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.jenson.fastfeederpro.INavigationdrawerController;
import com.jenson.fastfeederpro.INavigationdrawerControllerCallback;
import com.jenson.fastfeederpro.database.DbHelper;
import com.jenson.fastfeederpro.models.Article;
import com.jenson.fastfeederpro.network.Api;
import com.jenson.fastfeederpro.network.App;
import com.jenson.fastfeederpro.network.Apitags;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Jenson on 06-Feb-18.
 */

public class HomeActivityController implements INavigationdrawerController {
    Context context;
    INavigationdrawerControllerCallback inavigationcallback;

   // Apis apis;

    public HomeActivityController(Context context, INavigationdrawerControllerCallback inavigationcallback) {
        this.context = context;
        this.inavigationcallback = inavigationcallback;
//        apis = App.getClient().create(Apis.class);
    }

    @Override
    public void fetchFeed(final String url) {
        Retrofit retrofit = App.getClient();
        Api service = retrofit.create(Api.class);
        service.dataGet(url).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {


                    JsonObject responseObject = response.body();
                   // if (responseObject.has(Apitags.CATOGORY)) {
                        //JsonObject subcat = responseObject.getAsJsonObject(Apitags.CATOGORY);
                        JsonArray data = responseObject.getAsJsonArray(Apitags.DATA);
                        ArrayList<Article.Source> myorder = getGeneral(data.toString());

                        inavigationcallback.navigationsuccess(myorder,url);
                   /* }

                    else {
                        Toast.makeText(context,"error",Toast.LENGTH_LONG).show();
                    }*/
                }
                else {

                    Toast.makeText(context, "failed to fetch", Toast.LENGTH_LONG).show();

                }

            }
            private ArrayList<Article.Source> getGeneral(String s) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Article.Source>>() {
                }.getType();

                return gson.fromJson(s, listType);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                if(t instanceof SocketTimeoutException){
                    Toast.makeText(context, "No net", Toast.LENGTH_LONG).show();
                }

                if (t instanceof IOException) {
                    Toast.makeText(context, "this is an actual network failure :( inform the user and possibly retry", Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                }
                else {
                    Toast.makeText(context, "conversion issue! big problems :(", Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }


            }


        });

    }


}
