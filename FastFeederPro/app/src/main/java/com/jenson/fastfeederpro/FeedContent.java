package com.jenson.fastfeederpro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

public class FeedContent extends AppCompatActivity {
    ImageView iv;
    TextView tv,tvContent;
    WebView wv;
    CardView cv;

    private AVLoadingIndicatorView avi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_content);

        Bundle extras=getIntent().getExtras();
        String title=extras.getString("title");
        String thumb=extras.getString("thumb");
        String content=extras.getString("content");
        String contentURL=extras.getString("contentURL");

        iv=(ImageView)findViewById(R.id.iv2);
        Picasso.with(this).load(thumb).into(iv);
        tv=(TextView)findViewById(R.id.tv2);
        tv.setText(title);
        tvContent=(TextView)findViewById(R.id.tv3);
        tvContent.setText(content);
        wv=(WebView)findViewById(R.id.webview);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient());
        wv.loadUrl(contentURL);


        avi= (AVLoadingIndicatorView) findViewById(R.id.avi);

        //avi.hide();

        cv=(CardView)findViewById(R.id.cv);


        cv.clearAnimation();
        TranslateAnimation transAnim = new TranslateAnimation(0, 0, getDisplayHeight()/2,
                0);
        transAnim.setStartOffset(0);
        transAnim.setDuration(2000);
        transAnim.setFillAfter(true);
        transAnim.setInterpolator(new BounceInterpolator());
        transAnim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                        /*Log.i(TAG,
                                "Ending button dropdown animation. Clearing animation and setting layout");
                        bounceBallImage.clearAnimation();
                        final int left = bounceBallImage.getLeft();
                        final int top = bounceBallImage.getTop();
                        final int right = bounceBallImage.getRight();
                        final int bottom = bounceBallImage.getBottom();
                        bounceBallImage.layout(left, top, right, bottom);*/

            }
        });
        cv.startAnimation(transAnim);

    }

    private int getDisplayHeight() {
        return this.getResources().getDisplayMetrics().heightPixels;
    }
}
