package com.jenson.fastfeederpro;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.Toast;

public class About extends AppCompatActivity {
    WebView wv_about;
    ImageButton fb,wp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        wv_about=(WebView)findViewById(R.id.wv_about);

        String data = "<html><body bgcolor=\"#ADADAB\"><h6><marquee direction=\"up\" loop=\"true\"><br>This application is created by</br><br>Jenson John</br><br>All the APIs are property of <a href=\"http://newsapi.org\">newsapi.org</a></br><br>All right received &copy; 2018</br><br><b><center>Tools used</center></b></br><img src=\"http://icons.iconarchive.com/icons/martz90/hex/64/android-icon.png\"> <img src=\"http://icons.iconarchive.com/icons/designbolts/retro-3d-adobe/64/Adobe-Photoshop-CC-icon.png\"> <img src=\"http://icons.iconarchive.com/icons/google/chrome/64/Google-Chrome-icon.png\"> </marquee></h6></body></html>";
        wv_about.loadData(data, "text/html", "UTF-8");

        fb=(ImageButton)findViewById(R.id.fb);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String facebookUrl = "https://www.facebook.com/Jensonjohn001";
                try {
                    int versionCode = getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
                    if (versionCode >= 3002850) {
                        Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    } else {
                        Uri uri = Uri.parse("fb://profile/Jensonjohn001");
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
                }
            }
        });

        wp=(ImageButton)findViewById(R.id.wb);
        wp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager pm=getPackageManager();
                try {


                    Uri uri = Uri.parse("smsto:" + "919746998939");
                    Intent waIntent = new Intent(Intent.ACTION_SENDTO,uri);


                    PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");

                    startActivity(waIntent);

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }


               /* boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
                if (isWhatsappInstalled) {
                    Uri uri = Uri.parse("smsto:" + "919746998939");
                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, uri);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Hai Good Morning");
                    //sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                } else {
                    Toast.makeText(getApplicationContext(), "WhatsApp not Installed",
                            Toast.LENGTH_SHORT).show();
                    Uri uri = Uri.parse("market://details?id=com.whatsapp");
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(goToMarket);

                }*/




            }
        });
    }
   /* private boolean whatsappInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }*/
}
