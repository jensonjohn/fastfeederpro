package com.jenson.fastfeederpro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.jenson.fastfeederpro.adapters.HomeActivityAdapter;
import com.jenson.fastfeederpro.controllers.HomeActivityController;
import com.jenson.fastfeederpro.database.DbHelper;
import com.jenson.fastfeederpro.models.Article;
import com.jenson.fastfeederpro.session.SessionManager;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,INavigationdrawerControllerCallback {

    // Session Manager Class
    SessionManager session;

    AdView mAdView;

    private AVLoadingIndicatorView avi;
    DbHelper dbHelper;
    Context mContext;

    //to get headlines
    StringBuilder sb = new StringBuilder();
    WebView wv;

    ProgressBar progressBar;
    RecyclerView rv;
    private INavigationdrawerController inavdrawcontroller;
    private List<Article.Source> feedFromDB;

    ArrayList<Article.Source> offlinelist;
//tech
    private static final String techcrunch="v2/everything?sources=techcrunch&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String techradar="v2/everything?sources=techradar&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String wired="v2/everything?sources=wired&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String the_next_web= "v2/everything?sources=the-next-web&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String hacker_news="v2/everything?sources=hacker-news&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String ars_technica= "v2/everything?sources=ars-technica&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String crypto_coin= "v2/everything?sources=crypto-coins-news&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String engadget="v2/everything?sources=engadget&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String the_verge="v2/everything?sources=the-verge&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String recode="v2/everything?sources=recode&apiKey=ef5b910bb4a047d289c7366e96d9d9df";

//science
    private static final String scientist= "v2/everything?sources=new-scientist&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String next_big_feature= "v2/everything?sources=next-big-future&apiKey=ef5b910bb4a047d289c7366e96d9d9df";

//Entertainment
    private static final String mashable="v2/everything?sources=mashable&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String polygon="v2/everything?sources=polygon&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String entertainment_weekly="v2/everything?sources=entertainment-weekly&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String the_globe_and_mail="v2/everything?sources=the-globe-and-mail&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String the_lad_bible="v2/top-headlines?sources=the-lad-bible&apiKey=ef5b910bb4a047d289c7366e96d9d9df";


//news
    private static final String cbc_news="v2/everything?sources=cbc-news&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String cbs_news="v2/everything?sources=cbs-news&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String independent="v2/everything?sources=independent&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String abc_news= "v2/everything?sources=abc-news&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String al_jazeera= "v2/everything?sources=al-jazeera-english&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String fox_news= "v2/everything?sources=fox-news&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
///sports

    private static final String bleacher="v2/everything?sources=bleacher-report&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String footbal_italia="v2/everything?sources=football-italia&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String fox_sport="v2/everything?sources=fox-sports&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String bloomberg="v2/everything?sources=bloomberg&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
//business
    private static final String business_insider="v2/everything?sources=business-insider&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String cnbc="v2/everything?sources=cnbc&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String fin_post="v2/everything?sources=financial-post&apiKey=ef5b910bb4a047d289c7366e96d9d9df";
    private static final String fin_time="v2/everything?sources=financial-times&apiKey=ef5b910bb4a047d289c7366e96d9d9df";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Session class instance
        session = new SessionManager(getApplicationContext());

        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
        session.checkLogin();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // name
        String name = user.get(SessionManager.KEY_NAME);

        // email
        String email = user.get(SessionManager.KEY_EMAIL);

      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        String adMobID="ca-app-pub-5978049426916472~3932723034";
        MobileAds.initialize(this, adMobID);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        wv=(WebView)findViewById(R.id.wv);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


       // progressBar=(ProgressBar)findViewById(R.id.progressBar);

        avi= (AVLoadingIndicatorView) findViewById(R.id.avi);
        mContext=Home.this;

        rv=(RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rv.setLayoutManager(layoutManager);
        inavdrawcontroller = new HomeActivityController(getApplicationContext(), this);

        if(isNetworkAvailable()) {
            inavdrawcontroller.fetchFeed(techradar);
        }else{

            dbHelper=new DbHelper(mContext);
            offlinelist = dbHelper.getAllFeeds(techradar);
            setUpOfllineDatas(offlinelist ,techradar);

        }




        //To clear old data from DB
        dbHelper=new DbHelper(mContext);
       long count= dbHelper.get_row_count();

        if (count>1400){
            dbHelper.delete_old_datas();
            Snackbar.make(rv,"count : "+count,Snackbar.LENGTH_SHORT).show();
        }


    }
    @Override
    public void navigationsuccess(ArrayList<Article.Source> navdrawerlist,String feedId) {

        dbHelper=new DbHelper(mContext);
        dbHelper.saveFeed(navdrawerlist);

        String feedID = StringUtils.substringBetween(feedId, "=", "&");
        //Toast.makeText(mContext,feedID,Toast.LENGTH_SHORT).show();
        this.feedFromDB =dbHelper.getAllFeeds(feedID);
        HomeActivityAdapter adapter;
        //Load offline data if not internet connection
        if(!isNetworkAvailable()) {
            adapter = new HomeActivityAdapter(feedFromDB);
        }else{
            adapter = new HomeActivityAdapter(navdrawerlist);
        }


        //random color
        Random random = new Random();
        for (int i = 0; i < navdrawerlist.size(); i++) {
            // create a big random number - maximum is ffffff (hex) = 16777215 (dez)
            int nextInt = random.nextInt(256 * 256 * 256);
            // format it as hexadecimal string (with hashtag and leading zeros)
            String colorCode = String.format("#%06x", nextInt);

            sb.append("***<font color=" + colorCode + ">");
            sb.append(navdrawerlist.get(i).getTitle());
            sb.append("</font>***");
        }

        String headline= sb.toString();

        String data = "<html><body><h6><marquee>"+headline+"</marquee></h6></body></html>";
        wv.loadData(data, "text/html", "UTF-8");

       // progressBar.setVisibility(View.GONE);
        avi.hide();
        rv.setAdapter(adapter);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(Article.Source item) {

                Intent intent=new Intent(getApplicationContext(),FeedContent.class);
                intent.putExtra("title",item.getTitle().toString());
                intent.putExtra("thumb",item.getUrlToImage());
                intent.putExtra("content",item.getDescription());
                intent.putExtra("contentURL",item.getUrl());


                startActivity(intent);
                // Toast.makeText(Main2Activity.this, item.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_fav) {
            Intent i=new Intent(getApplicationContext(),About.class);
            startActivity(i);
            return true;
        } else if (id == R.id.action_logout) {
            // Clear the session data
            // This will clear all session data and
            // redirect user to LoginActivity
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Confirm");
            builder.setMessage("Are you sure Logout?");

            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing but close the dialog

                    session.logoutUser();


                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.techcrunch) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(techcrunch);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(techcrunch);
                setUpOfllineDatas(offlinelist ,techcrunch);
            }
        } else if (id == R.id.tech_radar) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(techradar);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(techradar);
                setUpOfllineDatas(offlinelist ,techradar);
            }
        } else if (id == R.id.wired) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(wired);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(wired);
                setUpOfllineDatas(offlinelist ,wired);
            }

        } else if (id == R.id.the_next_web) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(the_next_web);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(the_next_web);
                setUpOfllineDatas(offlinelist ,the_next_web);
            }

        } else if (id == R.id.hacker_news) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(hacker_news);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(hacker_news);
                setUpOfllineDatas(offlinelist ,hacker_news);
            }

        } else if (id == R.id.ars_technica) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(ars_technica);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(ars_technica);
                setUpOfllineDatas(offlinelist ,ars_technica);
            }

        } else if (id == R.id.crypto_coin) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(crypto_coin);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(crypto_coin);
                setUpOfllineDatas(offlinelist ,crypto_coin);
            }

        } else if (id == R.id.en_gadget) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(engadget);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(engadget);
                setUpOfllineDatas(offlinelist ,engadget);
            }

        } else if (id == R.id.the_verge) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(the_verge);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(the_verge);
                setUpOfllineDatas(offlinelist ,the_verge);
            }

        }else if (id == R.id.recode) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(recode);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(recode);
                setUpOfllineDatas(offlinelist ,recode);
            }

        }


        else if (id == R.id.cbc_news) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(cbc_news);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(cbc_news);
                setUpOfllineDatas(offlinelist ,cbc_news);
            }

        } else if (id == R.id.cbs_news) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(cbs_news);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(cbs_news);
                setUpOfllineDatas(offlinelist ,cbs_news);
            }

        } else if (id == R.id.independent) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(independent);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(independent);
                setUpOfllineDatas(offlinelist ,independent);
            }

        } else if (id == R.id.abc_news) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(abc_news);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(abc_news);
                setUpOfllineDatas(offlinelist ,abc_news);
            }

        } else if (id == R.id.al_jazeera) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(al_jazeera);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(al_jazeera);
                setUpOfllineDatas(offlinelist ,al_jazeera);
            }

        } else if (id == R.id.fox_news) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(fox_news);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(fox_news);
                setUpOfllineDatas(offlinelist ,fox_news);
            }

        }



        else if (id == R.id.mashable) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(mashable);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(mashable);
                setUpOfllineDatas(offlinelist ,mashable);
            }

        } else if (id == R.id.polygon) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(polygon);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(polygon);
                setUpOfllineDatas(offlinelist ,polygon);
            }

        } else if (id == R.id.entertainment_weekly) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(entertainment_weekly);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(entertainment_weekly);
                setUpOfllineDatas(offlinelist ,entertainment_weekly);
            }

        } else if (id == R.id.the_globe_and_mail) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(the_globe_and_mail);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(the_globe_and_mail);
                setUpOfllineDatas(offlinelist ,the_globe_and_mail);
            }

        } else if (id == R.id.the_lad_bible) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(the_lad_bible);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(the_lad_bible);
                setUpOfllineDatas(offlinelist ,the_lad_bible);
            }

        }


        else if (id == R.id.bleacher) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(bleacher);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(bleacher);
                setUpOfllineDatas(offlinelist ,bleacher);
            }

        } else if (id == R.id.footbal_italia) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(footbal_italia);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(footbal_italia);
                setUpOfllineDatas(offlinelist ,footbal_italia);
            }

        } else if (id == R.id.fox_sport) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(fox_sport);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(fox_sport);
                setUpOfllineDatas(offlinelist ,fox_sport);
            }

        } else if (id == R.id.bloomberg) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(bloomberg);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(bloomberg);
                setUpOfllineDatas(offlinelist ,bloomberg);
            }

        }

        else if (id == R.id.business_insider) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(business_insider);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(business_insider);
                setUpOfllineDatas(offlinelist ,business_insider);
            }

        } else if (id == R.id.cnbc) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(cnbc);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(cnbc);
                setUpOfllineDatas(offlinelist ,cnbc);
            }

        } else if (id == R.id.fin_post) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(fin_post);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(fin_post);
                setUpOfllineDatas(offlinelist ,fin_post);
            }

        } else if (id == R.id.fin_time) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(fin_time);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(fin_time);
                setUpOfllineDatas(offlinelist ,fin_time);
            }

        }

        else if (id == R.id.scientist) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(scientist);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(scientist);
                setUpOfllineDatas(offlinelist ,scientist);
            }

        } else if (id == R.id.next_big_feature) {
            if(isNetworkAvailable()) {
                inavdrawcontroller.fetchFeed(next_big_feature);
            }else{
                dbHelper=new DbHelper(mContext);
                offlinelist = dbHelper.getAllFeeds(next_big_feature);
                setUpOfllineDatas(offlinelist ,next_big_feature);
            }

        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //To check the state of internet connection
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setUpOfllineDatas(ArrayList<Article.Source> navdrawerlist,String feedId){

       // Toast.makeText(Home.this, "You are seeing offline data", Toast.LENGTH_SHORT).show();
        Snackbar.make(rv,"You are seeing offline data",Snackbar.LENGTH_LONG).show();

        dbHelper=new DbHelper(mContext);
        dbHelper.saveFeed(navdrawerlist);

        String feedID = StringUtils.substringBetween(feedId, "=", "&");
        //Toast.makeText(mContext,feedID,Toast.LENGTH_SHORT).show();
        this.feedFromDB =dbHelper.getAllFeeds(feedID);
        HomeActivityAdapter adapter;
        //Load offline data if not internet connection
        if(!isNetworkAvailable()) {
            adapter = new HomeActivityAdapter(feedFromDB);
        }else{
            adapter = new HomeActivityAdapter(navdrawerlist);
        }


        //random color
        Random random = new Random();
        for (int i = 0; i < navdrawerlist.size(); i++) {
            // create a big random number - maximum is ffffff (hex) = 16777215 (dez)
            int nextInt = random.nextInt(256 * 256 * 256);
            // format it as hexadecimal string (with hashtag and leading zeros)
            String colorCode = String.format("#%06x", nextInt);

            sb.append("***<font color=" + colorCode + ">");
            sb.append(navdrawerlist.get(i).getTitle());
            sb.append("</font>***");
        }

        String headline= sb.toString();

        String data = "<html><body><h6><marquee>"+headline+"</marquee></h6></body></html>";
        wv.loadData(data, "text/html", "UTF-8");

        // progressBar.setVisibility(View.GONE);
        avi.hide();
        rv.setAdapter(adapter);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(Article.Source item) {

                Intent intent=new Intent(getApplicationContext(),FeedContent.class);
                intent.putExtra("title",item.getTitle().toString());
                intent.putExtra("thumb",item.getUrlToImage());
                intent.putExtra("content",item.getDescription());
                intent.putExtra("contentURL",item.getUrl());

                startActivity(intent);


            }
        });


    }
}
