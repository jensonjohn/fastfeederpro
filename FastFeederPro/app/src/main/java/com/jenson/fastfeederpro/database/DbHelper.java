package com.jenson.fastfeederpro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jenson.fastfeederpro.models.Article;
import com.jenson.fastfeederpro.models.Users;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {



    public static final String DATABASE_NAME = "feed.db";
    private static final int DATABASE_VERSION = 1 ;

    public static final String TABLE_NAME = "Feeds";
    public static final String TABLE_USERS = "users";



    public static final String COLUMN_ID = "id";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_PASSWORD = "password";



    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_IMG_URL = "img_url";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_FEED_ID = "feed_id";



    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_TITLE + " TEXT UNIQUE, " +
                COLUMN_FEED_ID + " TEXT, " +
                COLUMN_IMG_URL + " TEXT UNIQUE, " +
                COLUMN_DESCRIPTION + " TEXT UNIQUE, " +
                COLUMN_TIME + " TEXT UNIQUE, " +
                COLUMN_URL + " TEXT UNIQUE);"
        );

        db.execSQL(" CREATE TABLE " + TABLE_USERS + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_EMAIL + " TEXT, " +
                COLUMN_PHONE + " TEXT, " +
                COLUMN_PASSWORD + " TEXT);"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        this.onCreate(db);
    }

    /**create record**/
    public void saveFeed(ArrayList<Article.Source> feedlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        for (int i=0;i<feedlist.size();i++){

        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, feedlist.get(i).getTitle());
        values.put(COLUMN_FEED_ID, feedlist.get(i).getSource().getId());
        values.put(COLUMN_IMG_URL, feedlist.get(i).getUrlToImage());
        values.put(COLUMN_DESCRIPTION, feedlist.get(i).getDescription());
        values.put(COLUMN_TIME,feedlist.get(i).getPublishedAt());
        values.put(COLUMN_URL, feedlist.get(i).getUrl());

        // insert
        db.insertWithOnConflict(TABLE_NAME,null, values,SQLiteDatabase.CONFLICT_REPLACE);
        }

        db.close();
    }

    public ArrayList<Article.Source> getAllFeeds(String feedId) {
        ArrayList<Article.Source> articlelList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME +" WHERE feed_id= '"+feedId+"'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Article.Source article = new Article.Source();
                article.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                article.setUrlToImage(cursor.getString(cursor.getColumnIndex(COLUMN_IMG_URL)));
                article.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
                article.setName(cursor.getString(cursor.getColumnIndex(COLUMN_FEED_ID)));
                article.setPublishedAt(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
                article.setUrl(cursor.getString(cursor.getColumnIndex(COLUMN_URL)));

                articlelList.add(article);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        return articlelList;
    }


    public void delete_old_datas(){

        String Query = String.format("DELETE FROM %s WHERE ROWID IN (SELECT ROWID FROM %s ORDER BY ROWID DESC LIMIT -1 OFFSET 1500)", TABLE_NAME, TABLE_NAME);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(Query);
            Log.d("SQL QUERY :",Query);
        }catch (Exception e){
            Log.d("SQL ERROR",e.toString());
            Log.d("SQL QUERY :",Query);
        }
        db.close();

    }

    public long get_row_count(){
        long count=0;

        String query="SELECT COUNT("+COLUMN_FEED_ID+") FROM "+TABLE_NAME ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

              count= cursor.getLong(0);

            } while (cursor.moveToNext());
        }
        Log.d("SQL QUERY :",query+" \nCount ="+count);

        // close db connection
        db.close();


        return count;
    }

    /**create record**/
    public void saveUser(Users users) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, users.getEmail()); // Contact username
        values.put(COLUMN_PHONE, users.getMob()); // Contact username
        values.put(COLUMN_PASSWORD, users.getPassword()); // Contact pass

        // Inserting Row
        db.insert(TABLE_USERS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public int checkUser(Users us)
    {
        int id=-1;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT id FROM users WHERE "+COLUMN_EMAIL+"=? AND "+COLUMN_PASSWORD+"=?",new String[]{us.getEmail(),us.getPassword()});
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            id=cursor.getInt(0);
            cursor.close();
        }
        return id;
    }


}
