package com.jenson.fastfeederpro;

import com.jenson.fastfeederpro.models.Article;

import java.util.ArrayList;

/**
 * Created by Jenson on 06-Feb-18.
 */

public interface INavigationdrawerControllerCallback {
    void navigationsuccess(ArrayList<Article.Source> navdrawerlist,String feedID);

}
