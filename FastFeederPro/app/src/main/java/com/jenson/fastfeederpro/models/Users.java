package com.jenson.fastfeederpro.models;

public class Users {
    long id;
    String email;
    String mob;
    String password;

    public Users(long id, String email, String mob, String password) {
        this.id = id;
        this.email = email;
        this.mob = mob;
        this.password = password;
    }

    public Users() {
    }

    public Users(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Users(String email, String mob, String password) {
        this.email = email;
        this.mob = mob;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
