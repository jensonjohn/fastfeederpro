package com.jenson.fastfeederpro;

import com.jenson.fastfeederpro.models.Article;

public interface OnItemClickListener {
    void onItemClick(Article.Source feed);
}