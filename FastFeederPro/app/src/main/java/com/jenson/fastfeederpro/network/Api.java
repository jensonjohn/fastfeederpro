package com.jenson.fastfeederpro.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface Api {

    @GET()
    Call<JsonObject> dataGet(@Url String url);

}