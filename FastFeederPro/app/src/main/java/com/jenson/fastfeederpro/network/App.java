package com.jenson.fastfeederpro.network;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jenson on 06-Feb-18.
 */

public class App extends Application {

    public static final String BASE_URL="https://newsapi.org/";

    static OkHttpClient.Builder httpClient = null;

    private static Retrofit retrofit = null;

    @Override
    public void onCreate() {
        super.onCreate();


    }

    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()
                .build();
       /* final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(06, TimeUnit.SECONDS)
                .connectTimeout(06, TimeUnit.SECONDS)
                .build();
        return okHttpClient;*/

    }

    public static Retrofit getClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(buildClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();
        }
        return retrofit;
    }


}
