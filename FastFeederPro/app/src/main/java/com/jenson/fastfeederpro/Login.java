package com.jenson.fastfeederpro;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jenson.fastfeederpro.animations.FlipAnimation;
import com.jenson.fastfeederpro.database.DbHelper;
import com.jenson.fastfeederpro.models.Users;
import com.jenson.fastfeederpro.session.SessionManager;

import java.util.List;

public class Login extends AppCompatActivity {
    EditText et_email,et_pass;
    EditText et_email1,et_pass1,et_mob;
    Button sign_in,sign_up;

    DbHelper db = new DbHelper(this);

    // Session Manager Class
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Session Manager
        session = new SessionManager(getApplicationContext());

        if(session.isLoggedIn()){
            Intent i =new Intent(getApplicationContext(),Home.class);
            startActivity(i);
            Login.this.finish();
        }


        et_email=(EditText)findViewById(R.id.email);
        et_pass=(EditText)findViewById(R.id.password);
        et_email1=(EditText)findViewById(R.id.email1);
        et_mob=(EditText)findViewById(R.id.phone);
        et_pass1=(EditText)findViewById(R.id.password1);
        sign_in=(Button)findViewById(R.id.sign_in);


        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email=et_email.getText().toString().trim();
                String password=et_pass.getText().toString().trim();

                int id= checkUser(new Users(email,password));
                if(id==-1) {

                    et_email.startAnimation(shakeError());
                    et_pass.startAnimation(shakeError());

                }
                else {

                    session.createLoginSession("App", email);


                    Intent i =new Intent(getApplicationContext(),Home.class);
                    startActivity(i);
                    Login.this.finish();
                }


            }
        });

        sign_up=(Button)findViewById(R.id.sign_up);


        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String email=et_email1.getText().toString().trim();
                String password=et_pass1.getText().toString().trim();
                String mob=et_mob.getText().toString().trim();


                int id= checkUser(new Users(email,password));
                if(id==-1)
                {
                    Snackbar.make(et_email1,"Registration complete!",Snackbar.LENGTH_SHORT).show();
                    // Inserting Contacts
                    Log.d("Insert: ", "Inserting ..");
                    db.saveUser(new Users( email, mob, password));
                    et_email.setText(email);
                    et_pass.setText(password);

                    flipCard();
                }
                else
                {
                    Snackbar.make(et_email1,"User exist!",Snackbar.LENGTH_SHORT).show();
                }






            }
        });

    }

    public int checkUser(Users user)
    {
        return db.checkUser(user);
    }

    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

    public void onCardClick(View view)
    {
        flipCard();
    }

    private void flipCard()
    {
        View rootLayout = (View) findViewById(R.id.root_view);
        View cardFace = (View) findViewById(R.id.outer_frame_login);
        View cardBack = (View) findViewById(R.id.outer_frame_signup);

        FlipAnimation flipAnimation = new FlipAnimation(cardFace, cardBack);

        if (cardFace.getVisibility() == View.GONE)
        {
            flipAnimation.reverse();
        }
        rootLayout.startAnimation(flipAnimation);
    }
}
