package com.jenson.fastfeederpro.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jenson.fastfeederpro.OnItemClickListener;
import com.jenson.fastfeederpro.R;
import com.jenson.fastfeederpro.animations.AnimationUtil;
import com.jenson.fastfeederpro.models.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jenson on 06-Feb-18.
 */

public class HomeActivityAdapter extends RecyclerView.Adapter<HomeActivityAdapter.CategorysubClass>  {
    private List<Article.Source> person;
    Context context;
    private OnItemClickListener onItemClickListener;
    int previousPosition=0;//for animation

    public HomeActivityAdapter(ArrayList<Article.Source> category) {
        this.person=category;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context=recyclerView.getContext();
    }
    @Override
    public HomeActivityAdapter.CategorysubClass onCreateViewHolder(ViewGroup parent, int viewType) {

        View  view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_content, parent, false);
        HomeActivityAdapter.CategorysubClass viewholder= new HomeActivityAdapter.CategorysubClass(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(HomeActivityAdapter.CategorysubClass holder, int position) {

        final Article.Source feedItem = person.get(position);
        holder.title.setText(feedItem.getTitle());
        holder.publishedAt.setText("Published at : "+feedItem.getPublishedAt());
        holder.feed_id.setText(feedItem.getName());

        //Render image using Picasso library
        if (!TextUtils.isEmpty(feedItem.getUrlToImage())) {
            Picasso.with(context).load(feedItem.getUrlToImage())
                    .error(R.drawable.ic_launcher_background)
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(holder.iv);
        }


            //for animate
            if(position>previousPosition){
                AnimationUtil.animate(holder,true);

            }else {
                AnimationUtil.animate(holder,false);
            }
            previousPosition=position;





        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(feedItem);
            }
        };
       holder.title.setOnClickListener(listener);
       holder.publishedAt.setOnClickListener(listener);
       holder.iv.setOnClickListener(listener);

    }

    @Override
    public int getItemCount() {
        return person.size();
    }

    public class CategorysubClass extends RecyclerView.ViewHolder {

        protected TextView title,publishedAt,feed_id;
        protected ImageView iv;


        public CategorysubClass(final View itemView) {
            super(itemView);


            title = (TextView)itemView.findViewById(R.id.title);
            publishedAt = (TextView)itemView.findViewById(R.id.publishedAt);
            feed_id = (TextView)itemView.findViewById(R.id.feed_id);
            iv=(ImageView)itemView.findViewById(R.id.thumbnail);

        }


    }
    public HomeActivityAdapter(List<Article.Source> person) {
        this.person=person;
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
